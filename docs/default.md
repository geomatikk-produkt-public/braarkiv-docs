# Velkommen til GeoDoc!

Vi er veldig glade for å kunne lansere GeoDoc!
* nytt og forbedret grensesnitt
* nytt og moderne API

> **GeoDoc (tidl. braArkiv) er en digital dokumentsamling for både skannet papirarkiv og importerte digitale arkiver fra sakarkiv og fagsystemer. GeoDoc kan tilpasses de fleste behov i offentlig og privat sektor.**

## Overgangsfase gammel og ny portal

Ettersom enkelte funksjoner ikke er helt klare enda, eller ikke vil videreføres i GeoDoc, vil begge portalene være tilgjengelig en stund fremover. Se fanen for "Roadmap" for en oversikt over kommende funksjonalitet.

Du har tilgang til den "gamle" portalen via gammel nettadresse som før: braarkiv.no/kunde

## Tilbakemeldinger

Vi ønsker dine tilbakemeldinger! Har du forslag til forbedringer eller ting du savner i GeoDoc kan du [sende oss en e-post](mailto:contact-project+geoikt-requests-42813401-issue-@incoming.gitlab.com)

Har du behov for vanlig support, [ta kontakt her](https://geomatikk.no/support/).

## Status og driftsmeldinger

[Gjeldende status og driftsmeldinger](https://arkiv.status.geoikt.no/)
