# Roadmap

## Valg av enkeltsider for nedlasting
Vi planlegger å videreføre nedlasting av enkeltsider fra detaljvisningen.

## Diverse annet
- Sortering på metadata med flere verdier
- Deling av dokumenter via Infoland
- Integrasjonssider (vil løses på annen måte)
- Masseredigering av metadata
- Massesletting
- Endre arkivdel
- Opprette, endre eller slette felles- og webservicebrukere
- Søk på DocID / record ID
- Vise og endre roller på grupper
