## Begreper og definisjoner i GeoDoc

### Arkiv 
Det øverste nivået i en arkivstruktur.

Tidligere omtalt som _basisklasse_ i braArkiv.

### Arkivdel
En logisk oppdeling for dokumenter som har felles metadata.

### Registrering
Det som tidligere ble omtalt som et dokument i braArkiv, kalles nå en registrering. Metadata legges på en registrering, slik som før. I GeoDoc kan en registrering kun inneholde ett (1) enkeltdokument.

### Dokument
Definisjon i følge NOARK5: "et dokument er et informasjonsobjekt som kan behandles som en enhet". De kaller det også et enkeltdokument. I GeoDoc kan en registrering kun inneholde ett (1) enkeltdokument. I praksis er dokumenter av type PDF, Microsoft Office eller bildeformater som JPG, TIFF eller lignende.

### Metadata
Strukturert informasjon om en registrering, f.eks et gårds- og bruksnummer (GID) eller en dokumentkategori.

Tidligere omtalt som _attributter_ i braArkiv.

### Metadatatype
En type metadata, f.eks tekst, dato, oppslagsverdi. Typene kan enten være enkelt- eller listeverdi.

Tidligere omtalt som _attributt-typer_ i braArkiv.

### Unntatt offentlighet
Et dokument som er unntatt offentlighet, er vurdert ut fra en lovhjemmel at det ikke åpent kan publiseres. 
I GeoDoc kan det være satt ved skanning, da det ble overført ved import av historiske baser eller satt via GeoIntegrasjon fra braArkiv Synk.

Tidligere omtalt som _gradert_ i braArkiv

### Skjermet dokument 
Et dokument skal være skjermet dersom det har innhold som skal være unntatt offentligheten eller inneholder personopplysninger.

Tidligere omtalt som _gradert_ i braArkiv.

### Ikke-publisert dokument
Et ikke-publisert dokument er et dokument som ikke vises i vår publikumsløsning doctorg. Årsaken er som oftest at kunden manuelt må bekrefte om dokumentet kan publiseres, dvs at det ikke har innhold skal være unntatt offentligheten eller inneholder personopplysninger, som f.eks fødselsnummer.

### Innsynslogg
Innsynsloggen viser hvem (brukere eller innbyggere) som har fått innsyn i spesifikke dokumenter i arkivet.

### Endringslogg
Endringsloggen viser hvilke brukere som har endret metadata på en registrering, endret et dokument eller gjort andre viktige endringer i arkivet.
