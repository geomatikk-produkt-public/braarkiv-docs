# Hva er nytt

## Siste oppdatering

### 12.6.2023 - GeoDoc
- **[Fiks]** Kolonner som omrokkeres skal nå huske sin rekkefølge (per sesjon)
- **[Fiks]** Rekkefølge på søkefelter var feil i enkelte tilfeller, nå korrigert
- **[Nytt]** En rekke små forbedringer og forenklinger på brukeropplevelse

## Nytt design
Våre brukere forteller oss at dagens braArkiv er meget enkelt å bruke. Derfor har vi som hovedfokus å videreføre dette.
Vi har innført et ryddigere grensesnitt, lettere fargepalett, forbedret universell utforming, støtte for nettbrett og mobil og flere andre designforbedringer.

## Forbedret søk

### Søk på tvers av arkiv- og arkivdeler
Vi har innført en ny arkivvelger som lar deg velge i hvilke arkiver (basisklasser) og arkivdeler man ønsker å søke. I tillegg er det støtte for søk på tvers av både arkiv og arkivdeler. Den vil vise de metadatatypene som er felles i søkefilter og søkeresultatet.

### Søkefilter

* Vi støtter nå `ELLER`-søk. I alle felter (utenom dato) kan man nå legge til flere verdier som vil gjøre et `ELLER`-søk.
* Alle endringer i arkivvelger og søkfilter reflekteres i URL. Man kan da kopiere URLen i nettleseren og f.eks sende til en annen bruker av arkivet, og de vil få opp samme resultater (dersom man har like rettigheter).

### Søkeresultat
* Et søk returnerer nå hele resultatet i arkivet (tidligere bare 200 dokumenter). Sortering gjøres dermed på hele resultatet, ikke bare inneværende resultat-side.
* Metadata vises nå ved dokumentets forhåndsvisning.
* Skjermede dokumenter markeres tydeligere i søkeresultat, i detaljvisning og ved deling.

### Detaljvisning dokument
* Ny detaljvisning med navigering og zoom.

## Deling av dokumenter via nedlasting, e-post og lenke
Nedlasting og e-post videreføres. I tillegg kan du nå generere en lenke med det dokumentet eller de dokumentene du ønsker å dele. Denne lenken gir tilgang til filen(e) i 7 dager.
Dessuten har vi gjort forbedringer når det gjelder stabilitet og ytelse på denne funksjonaliteten.

## Ytelse
Teknisk: Beta-arkivet benytter et nytt API basert på moderne teknologi. Dette gir oss forbedret ytelse på det meste. Vi jobber fortløpende med forbedringer og ny funksjonalitet.

## Endringslogg

### 31.5.2023 - GeoDoc
- **[Nytt]** Omdøpes til "GeoDoc"
- **[Nytt]** Mulighet til å endre eget passord
- **[Nytt]** Arkivdelvelgeren har enklere grensesnitt
- **[Nytt]** Diverse grensesnitt-elementer oppdatert

### 8.5.2023 - Rediger
- **[Nytt]** Forbedret interface for redigering av dokumenters metadata
- **[Fiks]** Fjernet diverse feilmeldinger i dokumentopprettelse

### 26.4.2023 - Opprett, Rediger, Søk
- **[Nytt]** Dokumenter med tilhørende metadata kan nå opprettes. Knappen finnes i menyen oppe til høyre (krever skrivetilgang)
- **[Nytt]** Dokumenter med tilhørende metadata kan nå redigeres. Redigering finnes på dokumentets detaljvisning (krever skrivetilgang)
- **[Nytt]** Nye søkefelter i venstre kolonne. Disse feltene tilbyr forbedret interaksjon.

### 23.3.2023 - Azure AD
Vi tilbyr nå mulighet for å logge seg på braArkiv via sin organisasjons Azure AD.

### 8.2.2023 - Gruppesjekkboks
- **[Nytt]** La til sjekkboks på grupperad. Den velger alle tilhørende dokumenter under grupperaden.

### 6.2.2023 - Forbedringer på arkivdelvelger
- **[Nytt]** Nytt utseende på arkivdelvelger
- **[Nytt]** La til knapp for hurtigvalg i arkivdelvelgeren

### 13.1.2023 - Grupper og brukere
- **[Nytt]** Det er nå mulig å liste, endre og opprette brukere, samt grupper

### 4.1.2023 - Fritekstsøk, innsynslogg for søk og diverse forbedringer
- **[Nytt]** Fritekstsøk introduseres som eget søkefelt
- **[Nytt]** Søk genererer nå innslag i innsynsloggen i braArkiv
- **[Nytt]** Ved å bruke CTRL+klikk på minibilde, åpnes detaljvisning i ny fane
- **[Nytt]** Paginering er justert til venstre med større tekst og totalt antall i bold
- **[Fiks]** Fikset feil i gruppering
- **[Fiks]** Liste med metadata i preview og detaljvisning bruker korrekt konfigurasjon slik dagens web allerede gjør

### 4.10.2022 - Gruppering
- **[Nytt]** Gruppering nå mulig med opp til 5000 søketreff
- **[Annet]** En lang rekke små optimaliseringer og forbedringer
